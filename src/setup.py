from setuptools import setup

from Hobbit.version import __version__

setup(
    name="Hobbit",
    version=__version__,
    author="Chemistry Computer Officers",
    author_email="support@ch.cam.ac.uk",
    description="Xymon/Hobbit extension script class",
    packages=["Hobbit"],
)
