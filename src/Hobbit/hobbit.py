from __future__ import print_function
import os
import subprocess
import time
import socket
import sys
import yaml


class Hobbit:
    colors = {"clear": 0, "green": 1, "purple": 2, "yellow": 3, "red": 4}

    def __init__(
        self, test="undefined", color="clear", hostname=None, text="", interval="15m"
    ):
        self.test = test
        self.text = text
        self.color = color
        if not hostname:
            hostname = socket.getfqdn()
        self.hostname = hostname
        self.interval = interval
        self.subtests = []
        self.load_config()

    def load_config(self):
        self.config = {}
        self.load_python_config()
        self.load_yaml_config()

    def list_config_files_with_extensions(self, extensions=[".yaml", ".yml"]):
        xymonhome = os.environ.get("XYMONHOME", "/usr/lib/xymon/client")
        config_path = os.path.join(xymonhome, "etc", "ext-cfg")
        config_dir = os.path.join(config_path, self.test + ".d")
        if os.path.isdir(config_dir):
            config_files = [
                os.path.join(config_dir, f)
                for f in os.listdir(config_dir)
                for extension in extensions
                if f.endswith(extension)
            ]
        else:
            config_files = [
                os.path.join(config_path, self.test + extension)
                for extension in extensions
            ]
        return config_files

    def load_python_config(self):
        config = {}
        config["__builtins__"] = {}
        config_files = self.list_config_files_with_extensions([".py"])
        for config_file in config_files:
            if os.path.isfile(config_file):
                exec(open(config_file).read(), config)
        self.config.update(config)

    def load_yaml_config(self):
        config = {}
        config_files = self.list_config_files_with_extensions([".yaml", ".yml"])
        for config_file in config_files:
            if os.path.isfile(config_file):
                with open(config_file, "r") as conf:
                    try:
                        newconf = yaml.safe_load(conf)
                    except AttributeError as e:
                        # Handle truly ancient PyYAML some of our users have on their webserver
                        if str(e) == "module 'collections' has no attribute 'Hashable'":
                            import collections.abc

                            collections.Hashable = collections.abc.Hashable
                            newconf = yaml.safe_load(conf)
                        else:
                            self.color_line(
                                "yellow",
                                "Failed to load YAML config file {config_file}",
                            )
                            newconf = None
                    if newconf is not None:
                        config.update(newconf)
        self.config.update(config)

    def max_color(self, a, b):
        if not (a in self.colors):
            raise RuntimeError("Silly colour used: " + a)
        if not (b in self.colors):
            raise RuntimeError("Silly colour used: " + b)
        return b if (self.colors[a] < self.colors[b]) else a

    def add_color(self, col):
        self.color = self.max_color(self.color, col)

    def line(self, text):
        self.text += text + "\n"

    def color_print(self, color, text):
        self.add_color(color)
        self.line(text)

    def color_line(self, color, text):
        self.color_print(color, "&" + color + " " + text)

    def send(self):
        t = time.localtime()
        date = time.asctime(t)
        self.incorporate_subtests()
        title = ""
        if self.color == "green":
            title = " - " + self.test + " OK"
        elif self.color == "yellow" or self.color == "red":
            title = " - " + self.test + " NOT ok"
        report = (
            "status+"
            + self.interval
            + " "
            + self.hostname
            + "."
            + self.test
            + " "
            + self.color
            + " "
            + date
            + " "
            + title
            + "\n"
            + self.text
        )
        if "BB" in os.environ and "BBDISP" in os.environ:
            proc = subprocess.Popen(
                [os.environ["BB"], os.environ["BBDISP"], "@"],
                stdin=subprocess.PIPE,
                shell=False,
            )
            proc.communicate(report.encode())
            proc.stdin.close()
        else:
            print(report)

    def moan(self, complaint):
        date = time.asctime(time.localtime())
        sys.stderr.write(
            date + " " + self.hostname + "." + self.test + ": " + complaint
        )
        self.color_line("yellow", "Warning: " + complaint)

    def croak(self, complaint):
        date = time.asctime(time.localtime())
        sys.stderr.write(
            date + " " + self.hostname + "." + self.test + ": " + complaint
        )
        self.color_line("yellow", "Warning: " + complaint)
        sys.exit(1)

    def add_subtest(self, title):
        self.subtests.append(Hobbit(title))
        return self.subtests[-1]

    # &subtest_i_color <a href=#subtest_i>subtest_i_title</a>
    # &subtest_j_color <a href=#subtest_j>subtest_j_title</a>
    # Original text from test
    # <a id='subtest_i'/>subtest_i_title
    # subtest_i_text
    # <br/>
    # a id='subtest_j'/>subtest_j_title
    # subtest_j_text
    # <br/>
    def incorporate_subtests(self, level=0):
        # Perform this for all subtests first.
        if len(self.subtests) == 0:
            return
        pre_lines = []
        post_lines = []
        i = 0
        for subtest in self.subtests:
            subtest.incorporate_subtests(level + 1)
            stcol = subtest.color
            self.add_color(stcol)
            stnam = subtest.test
            stid = f"subtest-{level}-{i}"
            pre_lines.append(f'&{stcol} <a href="#{stid}">{stnam}</a>')
            post_lines.append(f'<a id="{stid}">{stnam}</a>\n' + subtest.text)
            self.subtests = []
        br = "\n<br/>\n"
        self.text = "\n".join(pre_lines) + br + self.text + br + br.join(post_lines)
