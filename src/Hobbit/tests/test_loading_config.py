#! /usr/bin/python3
import unittest
import os
import Hobbit


class ConfigFileLoad(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        os.environ["XYMONHOME"] = os.path.join(
            os.path.dirname(__file__), "config_files"
        )

    def test_single_file_python(self):
        h = Hobbit.Hobbit("testd")
        assert h.config["sourcefile"] == "pythonsingle"

    def test_single_file_python_and_yaml(self):
        """
        When both YAML and .py config exists both load and YAML takes precedence
        """
        h = Hobbit.Hobbit("testa")
        assert h.config["sourcefile"] == "yamlsingle"

    def test_directory_python(self):
        h = Hobbit.Hobbit("testc")
        assert h.config["sourcefile"].startswith("python directory")

    def test_directory_python_and_yaml(self):
        h = Hobbit.Hobbit("testb")
        assert h.config["sourcefile"].startswith("yaml directory")

    def test_single_file_yaml_only(self):
        h = Hobbit.Hobbit("testf")
        assert h.config["sourcefile"] == "yamlsingle"

    def test_directory_yaml_only(self):
        h = Hobbit.Hobbit("testb")
        assert h.config["sourcefile"].startswith("yaml directory")
