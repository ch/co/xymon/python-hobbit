#! /usr/bin/python3
import unittest
import os
import Hobbit


class Subtests(unittest.TestCase):
    def test_single_file_python(self):
        h = Hobbit.Hobbit("testd")
        h.color_line("green", "all good")
        suba = h.add_subtest("disk1")
        subb = h.add_subtest("disk2")
        suba.color_line("red", "disk1 unhappy")
        subb.color_line("green", "disk2 happy")
        assert suba.color == "red"
        assert subb.color == "green"
        assert h.color == "green"
        h.incorporate_subtests()
        assert h.color == "red"
