%define shortname python3-hobbit
%define chver ch1

Name:           %{shortname}
Version:        %{version}
Release:        %{chver}
Summary:        Class for Xymon extensions in Python 3

Group:          Development/Tools
License:        GPL
URL:            https://gitlab.developers.cam.ac.uk/ch/co/xymon/python-hobbit/
Source0:        python3-hobbit-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch
Requires: python3, python36-PyYAML
BuildRequires: python3
BuildRequires:  python3-setuptools
BuildRequires:  python3-rpm-macros

%description
A class to help with writing Xymon extensions in Python 3.

This is Chemistry's package for the clusters.

%prep
%setup

%build
cd src
%{py3_build}


%install
cd src
#python3 setup.py install --root=%{buildroot}
%{py3_install}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{python3_sitelib}/Hobbit/
%{python3_sitelib}/Hobbit-%{version}-py%{python3_version}.egg-info/


%changelog
